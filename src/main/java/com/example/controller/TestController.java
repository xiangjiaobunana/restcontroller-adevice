package com.example.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.vo.TestVo;
import com.example.vo.User;

@RestController
public class TestController {
	
	private Logger log = LoggerFactory.getLogger(TestController.class);
	
	@RequestMapping(value = "/addUser",method = RequestMethod.POST)
	public String addUser(@RequestBody @Valid User user, 
			BindingResult bindingResult) {
		// 如果有参数校验失败，会将错误信息封装成对象组装在BindingResult里
        for (ObjectError error : bindingResult.getAllErrors()) {
        	log.info("--->"+error.getDefaultMessage());
            return error.getDefaultMessage();
        }
		return "6666";
	}
	
	@RequestMapping(value = "/addUser1",method = RequestMethod.POST)
	public String addUser1(@RequestBody @Valid User user) {
        
		return "6666";
	}
	
	@RequestMapping(value = "/test",method = RequestMethod.POST)
	public String test(@RequestBody @Valid TestVo testVo) {
        
		return "6666";
	}
}
