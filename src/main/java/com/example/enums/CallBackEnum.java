package com.example.enums;

public enum CallBackEnum {
	success(200,"ok"),
	validate_ex(10000,"参数校验失败");
	private Integer code;
	private String msg;
	
	public Integer getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	private CallBackEnum(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
}
