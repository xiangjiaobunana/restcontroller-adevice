package com.example.vo;

public class ResultVO<T> {
    private int code;
    private String msg;
    private T data;

    public ResultVO(T data) {
        this(1000, "success", data);
    }

    public ResultVO(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

	@Override
	public String toString() {
		return "ResultVO [code=" + code + ", msg=" + msg + ", data=" + data + "]";
	}
    
}
