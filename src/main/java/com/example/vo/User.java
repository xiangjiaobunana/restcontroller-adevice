package com.example.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class User implements Serializable {
	
	private static final long serialVersionUID = -2460669663541910243L;

	@NotNull(message = "用户id不能为空")
	private Integer id;
	
	@NotNull(message = "账号不允许为空")
	@Size(min = 6, max = 11, message = "账号长度必须是6-11个字符")
	private String account;
	
	@NotNull(message = "密码不允许为空")
	@Size(min = 6, max = 11, message = "密码长度必须是6-16个字符")
	private String password;
}
