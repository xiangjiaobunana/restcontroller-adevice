package com.example.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class TestVo implements Serializable {

	private static final long serialVersionUID = 7563633519927149180L;
	
	@NotNull(message = "test.msg.id")
	private Integer id;
	
	@NotNull(message = "test.msg.account")
	@Size(min = 6, max = 11, message = "test.msg.account.size")
	private String account;
	
	@NotNull(message = "test.msg.password")
	@Size(min = 6, max = 11, message = "test.msg.password.size")
	private String password;
}
