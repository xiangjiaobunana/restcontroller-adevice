package com.example.ex;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

public abstract class AbstractBaseController {
	
	@Autowired
	private MessageSource messageSource;
	
	public String getMessage(String key,String... strings ) {
		return this.messageSource.getMessage(key, strings, Locale.getDefault());
	}
}
